package com.boco.fastplot;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import com.ciklum.library.R;

/**
 * Draw millimeter grid on the screen and all support information for ECG signals
 */
public class GridView extends View {

    private static final String TAG = "PlotView";
    private int plotW = 0;
    private int plotH = 0;
    private int colorBG = Color.BLACK;
    private int colorGrid = Color.BLACK;
    private int colorText = Color.BLACK;
    private Paint paintLine;
    private Paint paintText;
    private double scale = 1;
    private double duration = 25;
    private double gain = 10;
//    private double fs = 500;


    public GridView(Context context) {
        super(context);
    }

    public GridView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.gridViewStyle);
    }

    public GridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initComponent(context, attrs);
        setLayerType(LAYER_TYPE_SOFTWARE, null);
    }

    private void initComponent(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.GridView, R.attr.gridViewStyle, 0);
        colorBG = a.getColor(R.styleable.GridView_backgroundColor, Color.BLACK);
        colorGrid = a.getColor(R.styleable.GridView_gridColor, Color.WHITE);
        colorText = a.getColor(R.styleable.GridView_textColor, Color.GRAY);
        a.recycle();

        paintText = new Paint();
        paintText.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        paintText.setTextSize(40);
        paintText.setColor(colorText);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.i(TAG, "onSizeChanged() [w, h]=[" + w + ", " + h + "]");
        this.plotH = h;
        this.plotW = w;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.i(TAG, "onDraw()");

        canvas.drawColor(colorBG);

//        double pixelTime = 1000/fs; // fs=500 => one pixel = 2msec
//        // Default values: 25mm/sec - 5mm = 200msec - 1mm = 40msec
//        // So, for fs=500 and scale=1 => step = 40/2=20pix
//        // Example: duration = 25mm/sec and fs=500 = 1mm * 25 = 1sec = 20p *2msec * 25
//        double step = 40*duration/25/pixelTime;
//        Log.w(TAG, "Speed: "+ duration + " mm/s, Gain:" + gain + " mm/mV" + " step=[" + step +"]px");
////        drawGrid(canvas, step);

//        double scaleX = duration/25;
        //Read how many pixels per one mm on this device
        float px = getPixelInmm();
        double step = px*scale;
        drawGrid(canvas, step);
        Log.i(TAG, "[w,h]=" + plotW + ", " + plotH +"] scale="+scale + "x, (1mm=" + px + "px), step= "+ step);

//        canvas.drawText("Speed: "+ duration + " mm/s", 10, 50, paintText);
//        canvas.drawText("Gain:  "+ gain + " mm/mV", plotW/2, 50, paintText);
        canvas.drawText("Scale: x"+ scale, 10, 90, paintText);

    }


    /**
     * Set multiplyer for 1mm grid. Default value = 1.
     * @param scale
     */
    public void setScale(double scale) {
        this.scale = scale;
        invalidate();
    }

    /**
     * Return how many pixels in 1mm on a screen
     * @return
     */
    public float getPixelInmm() {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, 1,
                getResources().getDisplayMetrics());
        return px;
    }

    private void drawGrid(Canvas canvas, double step) {
        Paint lineP = new Paint();
        lineP.setStrokeWidth(3);
        lineP.setColor(colorGrid);

        Paint lineDotedP = new Paint();
        lineDotedP.setARGB(255, 0, 0, 0);
        lineDotedP.setStyle(Paint.Style.STROKE);
        lineDotedP.setStrokeWidth(1);
        lineDotedP.setColor(colorGrid);
        float dotInterval = (float)step / 10;
        lineDotedP.setPathEffect(new DashPathEffect(new float[]{dotInterval, dotInterval}, 0));

        float x=0;
        while (x < plotW) {
            Path baseline = new Path();
            baseline.moveTo(x, 0);
            baseline.lineTo(x, plotH);
            canvas.drawPath(baseline, lineDotedP);
            x+= step;
        }

        float y=0;
        while (y < plotH) {
            Path baseline = new Path();
            baseline.moveTo(0,y);
            baseline.lineTo(plotW, y);
            canvas.drawPath(baseline, lineDotedP);
            y+= step;
        }

        x=0;
        while (x < plotW) {
            canvas.drawLine(x,0, x, plotH, lineP);
            x+=(step*5);
        }
        y=0;
        while (y < plotH) {
            canvas.drawLine(0,y, plotW, y, lineP);
            y+=(step*5);
        }
    }
    // https://stackoverflow.com/questions/21864863/android-canvas-clear-with-transparency
}

