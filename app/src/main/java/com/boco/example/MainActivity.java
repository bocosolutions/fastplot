package com.boco.example;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.boco.fastplot.GridView;
import com.boco.fastplot.PlotView;

import java.util.Random;

import com.boco.example.R;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private Runnable runnable;
    private boolean isRunnning = false;
    private PlotView plot;
    private GridView gridView;
    private TextView text;
    private io.socket.client.Socket socket;
    private double scaleX = 1;
    private double scaleY = 1;

    private double duration = 25;
    private double gain = 10;
    private double fs = 500;
    // Scale grid to adjust nice looking
    private double gridK = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        plot = findViewById(R.id.plotview);
        gridView = findViewById(R.id.gridview);
        text = findViewById(R.id.text);

        findViewById(R.id.btnPlay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isRunnning = !isRunnning;
                if (isRunnning) {
                    runService();
                }
            }
        });

        findViewById(R.id.btnAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                duration *=2;
                gridView.setScale(gridK);
                scaleX = getScaleX(duration, fs, gridView.getPixelInmm());
                text.setText(" Speed: " + duration + " mm/s");
                plot.setScaleX(scaleX*gridK);
            }
        });

        findViewById(R.id.btnDown).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                duration /=2;
                gridView.setScale(gridK);
                scaleX = getScaleX(duration, fs, gridView.getPixelInmm());
                text.setText(" Speed: " + duration + " mm/s");
                plot.setScaleX(scaleX*gridK);
//                gridView.setScale(scaleX);
            }
        });
//
//        try {
//                    socket = IO.socket("http://10.0.2.2:8088");
////        socket = IO.socket("http://192.168.1.220");
////        socket = IO.socket("http://172.24.8.11:8088");
////        socket = IO.socket("http://192.168.1.130");
//        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
//            @Override
//            public void call(java.lang.Object... args) {
//                Log.i(TAG, "SocketIO | EVENT_CONNECT!");
//            }
//        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
//            @Override
//            public void call(java.lang.Object... args) {
//                Log.i(TAG, "SocketIO | EVENT_DISCONNECT!");
//            }
//        }).on("stream", new Emitter.Listener() {
//            @Override
//            public void call(java.lang.Object... args) {
////                if (dataCallback != null) {
////                    dataCallback.ecg((JSONObject) args[0]);
////                }
//                final JSONObject data = (JSONObject) args[0];
//                AsyncTask.execute(new Runnable() {
//                    @Override
//                    public void run() {
//                        String key;
//                        JSONArray buffer;
//                        try {
//                            key = data.getString("key");
//                            buffer = data.getJSONObject("payload").getJSONArray("buf");
//                            Number[] buff = new Number[buffer.length()];
//                            for (int i=0; i < buffer.length(); i++) {
//                                buff[i] = (int) (-1*buffer.getDouble(i) * 100);
//                            }
//                            Log.i(TAG, "SocketIO |[" + key + "] size=" + buffer.length());
//                            if ("MLII".equals(key)) {
//                                // Key = MLII
//                                plot.addData(0, buff);
//                            }else {
//                                // Key = V2
//                                plot.addData(1, buff);
//                                plot.addData(2, buff);
//                                plot.addData(3, buff);
//                                plot.addData(4, buff);
//                                plot.addData(5, buff);
//                            }
//                        } catch (JSONException e) {
//                        }
//                    }
//                });
//            }
//        });
////        socket.connect();
//    } catch (URISyntaxException e) {
//        Log.e(TAG, "Problem with SocketIO creation", e);
//    }

        runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    int i = 0;
                    Random random = new Random();
                    while (isRunnning) {
                        int size = 100;
                        Number buf[] = new Number[size];
//                        double period = 16*1024/50;
//                        for (int j=0; j<size; j++) {
//                            double angle = 2.0 * Math.PI * (i+j) / period;
//                            int rand = random.nextInt(20 - 1 + 1) + 1;
//                            int pik = j == size/2 ? 100 :0;
//                            buf[j] = (int) (Math.sin(angle) * 127f + rand + pik);
//                        }

                        for (int j=0; j<size; j++) {
                            int rand = random.nextInt(20 - 1 + 1) + 1;
                            int pik = j == size/2 ? 100 : 0;
                            if (j<size/2) {
                                buf[j] = 127f + rand + pik;
                            } else {
                                buf[j] = -127f - rand -pik;
                            }
                        }
//                        for (int j=0; j<size; j++) {
//                            buf[j] = (byte) ((i+j) % 100);
//                        }
                        i += size;
                        Thread.sleep(100);
                        plot.addData(0, buf);
//                        plot.addData(1, buf);
//                        plot.addData(1, buf);
//                        plot.addData(2, buf);
//                        plot.addData(3, buf);
//                        plot.addData(4, buf);
//                        plot.addData(5, buf);
//                        plot.addData(6, buf);
//                        plot.addData(7, buf);
//                        plot.addData(8, buf);
//                        plot.addData(9, buf);
//                        plot.addData(10, buf);
//                        plot.addData(11, buf);
//                        isRunnning = false;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        //bindService(new Intent(this, ConnectionService.class), serviceConnection, Context.BIND_AUTO_CREATE);
    }

//    private void calculateLabels(int scaleX) {
//        int fs = 500; // Sampling rate
//        int plotW = plot.getWindowW();
//        int plotH = plot.getWindowH();
//
//        int cellSize = 10; // number of pixels per cell == 1mm
//
//        // Example: duration = 25mm/sec and fs=500 = 1mm * 25 = 1sec = 20p *2msec * 25
////        int step = (int)(1000/duration*1000/fs);
//    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        text.setText(" Speed: " + duration + " mm/s");
        gridView.setScale(gridK);
        scaleX = getScaleX(duration, fs, gridView.getPixelInmm());
        plot.setScaleX(scaleX*gridK);
        // Clear buffers
        plot.resetPlotBuffer();
        plot.startDrawing();
        // Connect to data source
        runService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
        stopService();
        plot.stopDrawing();
    }

    private void runService() {
        isRunnning = true;
        AsyncTask.execute(runnable);
    }

    private void stopService() {
        isRunnning = false;
    }

    private double getScaleX(double duration, double fs, double px_mm) {
        double pointsInmm = fs / duration;
        double scaleX = px_mm / pointsInmm;
        Log.i(TAG, "duration=" + duration + "px=" + px_mm + ", pointsInmm=" + pointsInmm + ", scaleX=" + scaleX);
        return scaleX;
    }
}
