package com.boco.fastplot;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.ciklum.library.R;

import java.util.ArrayList;
import java.util.List;

public class PlotView extends SurfaceView implements SurfaceHolder.Callback{

    private static final String TAG = "PlotView";
    private List<Paint> paintList;
    private BuffersController buffersController;
    private int bufferSize = 0;
    private DrawThread drawThread;
    private int plotW = 0;
    private int plotH = 0;
    private double scaleX = 1;
    private double scaleY = 1;
    private int lineNumbers = 1;
    private List<Point> lastPoints;
    private int eraseWindowSize = 100;


    public PlotView(Context context) {
        super(context);
        // No Attributes. Only One chanel by default
        initComponents(1, 1000);
    }

    public PlotView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.plotViewStyle);
    }

    public PlotView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getParamsFromAttributes(context, attrs);
        initComponents(lineNumbers, bufferSize);
        resetPlotBuffer();
    }

    /**
     * Stop drawing thread.
     */
    public void stopDrawing() {
        if (drawThread != null) {
            drawThread.setRunning(false);
        }
    }

    /**
     * Start drawing thread
     */
    public void startDrawing() {
        drawThread = new DrawThread(getHolder());
        drawThread.setRunning(true);
        drawThread.start();
    }

    /**
     * Clear all data from internal buffers
     */
    public void resetPlotBuffer() {
        if (buffersController != null) {
            buffersController.clearAllBuffers();
        }
    }


    private void getParamsFromAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.PlotView, R.attr.plotViewStyle, 0);
        this.lineNumbers = a.getInteger(R.styleable.PlotView_channelNumber, 1);
        this.bufferSize =  a.getInteger(R.styleable.PlotView_bufferSize, 1000);
        a.recycle();
    }

    private void initComponents(int numChannels, int buffSize) {
        setNumberOfChannels(numChannels, buffSize);
        getHolder().addCallback(this);
        getHolder().setFormat(PixelFormat.TRANSPARENT);
        this.setZOrderOnTop(true);
    }

    private int[] getColorList() {

//        context.getResources().getA
        int[] colorList = {
                Color.rgb(1,107,78), //#016B4E
                Color.rgb(0,99,159), //#00639F
                Color.rgb(236,119,31), //#EC771F
                Color.rgb(0,136,148), //#008894
                Color.rgb(117,101,169), //#7565A9
                Color.rgb(240,147,28)}; //#F0931C
        return colorList;
    }

    /**
     * Important to init Channels before start drawing!
     * @param numChannels
     * @param buffSize
     */
    private void setNumberOfChannels(int numChannels, int buffSize) {
        this.lineNumbers = numChannels;
        buffersController = new BuffersController(lineNumbers, buffSize);
        this.paintList = new ArrayList<>(lineNumbers);
        this.lastPoints = new ArrayList<>(lineNumbers);

        int[] colorList = getColorList();
        for (int i=0; i<lineNumbers; i++) {
            Paint paint = new Paint();
            paint.setColor(colorList[i%lineNumbers]);
            paint.setStrokeWidth(3);
            paintList.add(paint);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.i(TAG, "onSizeChanged() [w, h]=[" + w + ", " + h + "]");
        this.plotH = h;
        this.plotW = w;

        // Set initial points for drawing.
        for (int i=0; i<lineNumbers; i++) {
            lastPoints.add(new Point(0, plotH*(i+1)/(lineNumbers+1)));
        }
    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.i(TAG, "surfaceCreated()");
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        Log.i(TAG, "surfaceChanged() [i, i1, i2]=[" + i + ", " + i1 + ", " + i2 + "]");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        Log.i(TAG, "surfaceDestroyed()");
        boolean retry = true;
        drawThread.setRunning(false);
        while (retry) {
            try {
                drawThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.i(TAG, "onDraw()");
    }

    private void drawLine(Canvas canvas, int chanel,  Number[] buffer, float y_position) {

        Point startP = lastPoints.get(chanel);
//        Log.d(TAG, "drawLines() offset [x,y]=[" + startP.getX() + ", " + startP.getY() + "]");
        float startX=startP.getX();
        float startY=startP.getY();
        float stopX=startX;
        float stopY=startY;

        if (buffer != null) {
            float step = 0;
            for (int i = 0; i < buffer.length; i++) {
                stopX = startX + (float) scaleX;
                stopY = y_position + buffer[i].intValue() * (float) scaleY;
//                if (i==0 || i==buffer.length-1) {
//                    Log.d(TAG, "[" + startX + " ," + startY + " - " + stopX + ", " + stopY + "]");
//                }
                if (stopX > plotW) {
                    startX = 0; // out of draw window. Start draw from beginning
                    stopX = 0;
//                    Log.i(TAG, "CLEAN ALL() stopX=" + stopX + ", stopY=" + stopY);
                } else {
//                    Log.d(TAG, "[" + startX + " ," + startY + " - " + stopX + ", " + stopY + "]");
                    canvas.drawLine(startX, startY, stopX, stopY, paintList.get(chanel));
                    startX += (float) scaleX;
                }
                startY=stopY;
            }
            lastPoints.set(chanel, new Point(stopX, stopY));
        }
    }

    /**
     * Drawing thread continiously
     */
    class DrawThread extends Thread {
        private boolean running = false;
        private SurfaceHolder surfaceHolder;

        public DrawThread(SurfaceHolder surfaceHolder) {
            this.surfaceHolder = surfaceHolder;
        }

        public void setRunning(boolean running) {
            this.running = running;
        }

        @Override
        public void run() {
            Canvas canvas;
            while (running) {
                canvas = null;
                try {
                    // Wait till next draw
                    // bufferBalancer.tuneSpeed();
                    int delay = buffersController.getDelay();
                    Thread.sleep(delay);
//                    int Np = buffersController.getNpToDraw();
//                    if (Np > eraseWindowSize/2) {
//                        Log.e(TAG, "MORE than window!!!");
//                        Np = eraseWindowSize/2;
//                    }
                    int Np = buffersController.getNpToDraw();
                    drawChannelsData(surfaceHolder, Np);
//                    Log.d(TAG, "canvas [W,H]=[" + canvas.getWidth() + ", " + canvas.getHeight() + "]");
//                    Log.d(TAG, "canvas.isHardwareAccelerated=" + canvas.isHardwareAccelerated());
                } catch (Exception e) {
                    Log.e(TAG, "Cant's sleep:", e);
                } finally {
                    if (canvas != null) {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }

    private void drawChannelsData(SurfaceHolder surfaceHolder, int size) {
        // Get data to draw. If null nothing to draw just Skip
        Number[][] data = buffersController.getData(size);
        if (data != null) {
            Log.d(TAG, "DRAW chanels=" + data.length + ", K=" + buffersController.getBuffersLoad());
            // Lock part of screen to draw
            float lastX = lastPoints.get(0).getX();
            Canvas canvas = null;
            try {
                if (lastX + eraseWindowSize < plotW) {
                    // When where is a space for normal size erace window.
                    Rect rect = new Rect((int)lastX, 0, (int)lastX + eraseWindowSize+1, plotH);
                    canvas = surfaceHolder.lockCanvas(rect);
                    // TODO: TRY on API 26
                    // canvas = surfaceHolder.lockHardwareCanvas(rect);
                    if (canvas != null) {
                        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                        for (int i = 0; i < lineNumbers; i++) {
                            drawLine(canvas, i, data[i], plotH * (i + 1) / (lineNumbers + 1));
                        }
                    }
                }
                else {
                    Rect rect = new Rect((int)lastX, 0, plotW, plotH);
                    canvas = surfaceHolder.lockCanvas(rect);
                    if (canvas != null) {
                        int wSize = plotW - (int) lastX;
                        // Log.i(TAG, "wSize=" + wSize + ", dataLen=" + data.length);


                        if (data[0].length >= wSize) {
                            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                            for (int i = 0; i < lineNumbers; i++) {
                                Number[] data1 = new Number[wSize];
                                System.arraycopy(data[i], 0, data1, 0, wSize);

                                //                                Log.i(TAG, "DRAW part1 OK");
                                drawLine(canvas, i, data1, plotH * (i + 1) / (lineNumbers + 1));
                            }
                            surfaceHolder.unlockCanvasAndPost(canvas);

                            int part2Size = data[0].length - wSize;
                            Rect rect2 = new Rect(0, 0, part2Size, plotH);
                            canvas = surfaceHolder.lockCanvas(rect2);
                            if (canvas != null) {
                                canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                            }
                            for (int i = 0; i < lineNumbers; i++) {
                                Number[] data2 = new Number[part2Size];
                                System.arraycopy(data[i], wSize, data2, 0, part2Size);
                                if (canvas != null) {
                                    //                                Log.i(TAG, "DRAW part2 OK - part2Size=" + part2Size);
                                    drawLine(canvas, i, data2, plotH * (i + 1) / (lineNumbers + 1));
                                }
                            }
                        } else {
                            //                      Log.i(TAG, "All OK - no need seccond surface");
                            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                            for (int i = 0; i < lineNumbers; i++) {
                                drawLine(canvas, i, data[i], plotH * (i + 1) / (lineNumbers + 1));
                            }
                        }
                    }
                }
            } finally {
                if (canvas != null) {
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }

    /**
     * Add data array to the buffer
     * @param chanelNumber
     * @param buffer
     */
    public void addData(int chanelNumber, Number[] buffer) {
        buffersController.addData(chanelNumber, buffer.clone());
        if (chanelNumber == 0) {
            Log.d(TAG, "BuffersLoad=[" + (buffersController.getBuffersLoad()) +
                    "], is=" + buffersController.isBufferReady() +
                    " points=" + buffersController.getNumberOfPoints());
        }
    }

    /**
     * Set multiplayer for X axis. Default = 1;
     * @param scaleX
     */
    public void setScaleX(double scaleX) {
        this.scaleX = scaleX;
    }

    /**
     * Set multiplayer for Y axis. Default = 1;
     * @param scaleY
     */
    public void setScaleY(double scaleY) {
        this.scaleY = scaleY;
    }

    public int  getWindowW() {
        return plotW;
    }

    public int  getWindowH() {
        return plotH;
    }
    // https://stackoverflow.com/questions/21864863/android-canvas-clear-with-transparency
}

