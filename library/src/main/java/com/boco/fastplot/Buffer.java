package com.boco.fastplot;

/**
 * This class implement circle buffer
 */
public class Buffer {

    private static final String TAG = "Buffer";

    private Number[] buffer;
    private int bufferSize = 0;
    private int end = 0; // Show position where no data, so don't try to read from this pos.
    private int cur = 0; // Show position where to start reading data
    private long startTime = -1;
    private long totalData = 0;
    private double totalSpeed = 0;

    public Buffer(int size) {
        this.buffer = new Number[size];
        this.bufferSize = size;
    }

    /**
     * Reset cur and end position and all
     */
    public void clear() {
        this.cur = 0;
        this.end = 0;
        totalSpeed = 0;
        startTime = -1;
        totalData = 0;
    }

    public int addData(Number[] data) {
        int errorCode = 0;
        int newDataSize = data.length;
//        Log.d(TAG, "addData() [cur,end]=[" + cur + ", "+ end +"]");
        if (newDataSize < (bufferSize - getNumberOfPoints())) {

            if ((end + newDataSize) <= bufferSize) {
                // if data not cross buffer border - just copy.
                System.arraycopy(data, 0, buffer, end, newDataSize);
                end += newDataSize;
            } else {
                int part1Size = bufferSize - end;
                int part2Size = newDataSize - part1Size;
                System.arraycopy(data, 0, buffer, end, part1Size);
                end = 0;
                System.arraycopy(data, part1Size, buffer, end, part2Size);
                end += part2Size;
            }

            totalData += data.length;
            if (startTime == -1) {
                startTime = System.currentTimeMillis();
            } else {
                long time = System.currentTimeMillis();
                if (time != startTime) {
                    totalSpeed = totalData * 1000 / (time - startTime);
                }
            }
        } else {
            // No space in buffer to add more data
            errorCode = -1;
        }
        //Log.i(TAG, "[BufferLoad=" + getBufferLoad() + ", Np=" + Np + ", D=" + delay + "]");
        //lastLoad = getBufferLoad();
        return errorCode;
    }

//    /**
//     * Read data according to calculated Np - Number of points calculated by balancer
//     * @return
//     */
//    public Number[] getData(){
//        int n = Math.min(getNumberOfPoints(), Np);
//        return getData(n);
//    }

    /**
     * Return null if requested data size > that data in buffer
     * @param size
     * @return
     */
    public Number[] getData(int size) {
        Number[] result = null;
        if (size > 0 && size <= getNumberOfPoints()) {
            result = new Number[size];
            if ((cur + size) <= bufferSize) {
                // if data not cross buffer border - just copy.
                System.arraycopy(buffer, cur, result, 0, size);
                cur += size;
            } else {
                int part1Size = bufferSize - cur;
                int part2Size = size - part1Size;
                System.arraycopy(buffer, cur, result, 0, part1Size);
                cur = 0;
                System.arraycopy(buffer, cur, result, part1Size, part2Size);
                cur += part2Size;
            }
//            Log.d(TAG, "buff [" + result[0] + ", " + result[result.length-1] + "]" );
        }
        return result;
    }

    /**
     * Used only for Unit Tests.
     */
    public Number[] test_getAllBufferData() {
        return buffer;
    }

    /**
     * Return K in range 0 - 100
     * 0.0 - Empty,
     * 1.0 - Full
     * @return 0 - All is Ok! -1 - Buffer overloaded.
     */
    public double getBufferLoad() {
        int S = getNumberOfPoints();
        double k = (1.0 * S) / bufferSize;
//        Log.d(TAG, "getBufferLoad() = " + k + ", points=" + S);
        return k;
    }

    /**
     * Return number of point in buffer
     * @return
     */
    public int getNumberOfPoints() {
        int S = end - cur;
        if (S < 0) {
            S += bufferSize;
        }
        return S;
    }

    public double getTotalSpeed() {
        return totalSpeed;
    }
}