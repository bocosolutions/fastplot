package com.boco.fastplot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BuffersController {

    private static final String TAG = "BuffersController";
    private int numberOfBuffers = 0;
    private int bufferSize = 0;
    private List<Buffer> bufferList;
    private double minK = 0;
    private boolean isAddZeros = false;
    private int delay = 20;
    private int Np = 20;

    public BuffersController(int numberOfBuffers, int bufferSize) {
        this.bufferSize = bufferSize;
        bufferList = new ArrayList<>(numberOfBuffers);
        this.numberOfBuffers = numberOfBuffers;
        for (int i=0; i<bufferSize; i++) {
            bufferList.add(new Buffer(bufferSize));
        }
    }

    public void clearAllBuffers() {
        Number[] zeroBuff = new Number[(int)(bufferSize*minK)+1];
        Arrays.fill(zeroBuff, 0);
        for (Buffer buff : bufferList) {
            buff.clear();
            if (isAddZeros) {
                buff.addData(zeroBuff);
            }
        }

    }

    /**
     * Add data to the chanel's buffer.
     *
     * @param chanelNumber
     * @param data array of @Number
     * @return errorCode = 0 - OK; -1 not enough space in buffer.; -2 buffer not ready
     */
    public int addData(int chanelNumber, Number[] data) {
        if (bufferList.size() == 0) {
            return -2;
        } else {
            return bufferList.get(chanelNumber).addData(data);
        }
    }

    /**
     * Return Matrix of data to draw; [chanelNumber, data];
     * null - if there is not enughth data to draw. Even if it's not enough in one chanell.
     * @return
     */
    public Number[][] getData(int size) {
        Number[][] result = null;
        boolean isReady = isBufferReady();
        if (isReady) {
            result = new Number[numberOfBuffers][];
            for (int i=0; i<numberOfBuffers; i++) {
                result[i] = bufferList.get(i).getData(size);
                if (result[i] == null) {
                    result = null;
//                    Log.e(TAG, "Error! Chanel " + i +" has no data");
                    break;
                }
            }
        }
        tuneSpeed();
//        Log.d(TAG, "BuffersLoad=[" + (getBuffersLoad()) + "], is=" + isReady+", D=" + delay + ", Np=" + Np +
//                " points=" + bufferList.get(0).getNumberOfPoints() +
//                ", Speed=" + bufferList.get(0).getTotalSpeed());
//        if (lastLoad < 0.1) {
//            Log.e(TAG, "isReady=" + isReady);
//            getBuffersLoad();
//        }
        return result;
    }

    public boolean isBufferReady() {
        boolean result = true;
        for (int i=0; i<numberOfBuffers; i++) {
            result = bufferList.get(i).getBufferLoad() > minK;
            if (!result) break;
        }
        return result;
    }

    public double getBuffersLoad() {
        double result = 0;
        double[] debug = new double[numberOfBuffers];
        for (int i=0; i<numberOfBuffers; i++) {
            debug[i] = bufferList.get(i).getBufferLoad();
            result += debug[i];

        }
        result /= numberOfBuffers;
//        if (result < 0.01) {
//            Log.e(TAG, "result=" + result);
//            for (int i=0; i<numberOfBuffers; i++) {
//                debug[i] = bufferList.get(i).getBufferLoad();
//                Log.e(TAG, "Chanel["+i+"]=" + debug[i] + ", points=" + bufferList.get(i).getNumberOfPoints());
//            }
//        }
        return result;
    }

    public int getNumberOfPoints() {
        int result = 0;
        for (int i=0; i<numberOfBuffers; i++) {
            result += bufferList.get(i).getNumberOfPoints();
        }
        result /= numberOfBuffers;
        return result;
    }

    public int getDelay() {
        return delay;
    }

    public int getNpToDraw() {
        return Np;
    }

    public void tuneSpeed() {

//        double speed = bufferList.get(0).getTotalSpeed();
//        double load = bufferList.get(0).getBufferLoad();
//        Np = (int) (speed * delay / 1000) + 1;
//        // Add correction if buffer size go up or down
//        Np = (int) (Np * (1+load)) + 1;
//
//        if (load > 0.75) {
//            Np *= 1.5;
//        }
    }

}